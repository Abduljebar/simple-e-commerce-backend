class Product{
    constructor(
         name,
         price,
         id,
         photo,
         rate,
         reviewer,
         category,
         description
    ){
        this.name = name;
        this.price = price;
        this.id = id;
        this.photo = photo;
        this.rate = rate;
        this.reviewer = reviewer;
        this.category = category;
        this.description = description;
    }
}

module.exports = Product;