let products = [
  {
    name: "Coffee",
    photo:
      "https://images.squarespace-cdn.com/content/v1/57b88db03e00be38aec142b0/1506786374395-FN0E1UMWK3BJEPJUYHFK/13_Ethiopian+New+Year+Addis+Ababa+HandZaround.jpg?format=1000w",
    id: "12",
    price: "10",
    rate: "4.7",
    reviewer: "1290",
    category: "food",
    description: "a little description about product.",
  },
  {
    name: "Kemis",
    photo:
      "https://i.pinimg.com/originals/61/b0/32/61b032f62b66276be1d3648a3b83413c.jpg",
    id: "14",
    price: "180",
    rate: "4.3",
    reviewer: "230",
    category: "clothes",
    description: "a little description about product.",
  },
  {
    name: "Arsi Kemis",
    photo:
      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ-EBwQOs503-uoEo1EGdqNuT_IcesAmQ-r495XieChT7WpZ5bFW5hbESqslNcvtdn1isc&usqp=CAU",
    id: "16",
    price: "13",
    rate: "4.5",
    reviewer: "290",
    category: "clothes",
    description: "a little description about product.",
  },
  {
    name: "Genna Sport Stick",
    photo:
      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTvKOgZRNwtM_O7fc8qU5R48pG7LYQnsC6Dtg&usqp=CAU",
    id: "18",
    price: "15",
    rate: "4.6",
    reviewer: "170",
    category: "home",
    description: "a little description about product.",
  },
  {
    name: "Shiro",
    price: "10",
    id: "18",
    photo:
      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRWBN_lwy_BOErVhVhtPZpqeU3AKPnhrAnVjcn9dTvYJpt3Lb2ebwRG3bc_-ZENFvZpi8U&usqp=CAU",
    rate: "4",
    reviewer: "1233",
    category: "food",
    description: "this is description about shiro",
  },
  {
    name: "Mesob",
    price: "121",
    id: "182",
    photo:
      "http://res.cloudinary.com/link-ethiopia/image/upload/v1435705436/cgr-1184-X2_qcmax8.jpg",
    rate: "5",
    reviewer: "1222",
    category: "home",
    description: "this is description about mesob",
  },
];

module.exports = products;
