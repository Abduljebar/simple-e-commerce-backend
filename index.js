const express = require('express');
const cors = require('cors')
const bodyParser = require('body-parser');
const productRoute = require('./routes/product-route');
const userRoute = require('./routes/user-route');
const http = require('http');
const { addUser, getUsers, getUser } = require('./controllers/user-controller');
const { nextTick } = require('process');


const app = express();
const port = process.env.port | 4000;

app.use(express.urlencoded({extended:true}));
app.use(cors());
app.use(bodyParser.json())
app.use(express.json());

app.use((req,res,next)=>{
    console.log(req.originalUrl);
    next();
})
app.use('/api',productRoute);
app.use('/api',userRoute);

// var server = http.createServer(function(req, res) {
//     res.writeHead(200, {'Content-Type': 'text/plain'});
//     var message = 'It works!\n',
//         version = 'NodeJS ' + process.versions.node + '\n',
//         response = [message, version].join('\n');
//     res.end(response);
// });
// server.listen();
// var server = http.createServer(app);
// server.listen();


app.listen(port, ()=> console.log(`listening on port ${port}`));
