const express = require('express');
const { addProduct, getProducts, getProduct, updateProduct, deleteProduct } = require('../controllers/product-controller');


const router = express.Router();

router.post('/product',addProduct);
router.get('/products',getProducts);
router.get('/product/:id',getProduct);
router.put('/product/:id',updateProduct);
router.delete('/product/:id',deleteProduct);

module.exports =     router
