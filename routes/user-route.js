
const express = require('express');
const { addUser, getUsers, getUser } = require('../controllers/user-controller');

const router = express.Router();

router.post('/user',addUser);
router.get('/users',getUsers);
router.get('/user/:id',getUser);

module.exports = router;
