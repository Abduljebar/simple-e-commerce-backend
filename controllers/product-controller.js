let products = require("../db");
let Product = require("../models/product");

const addProduct = (req, res, next) => {
  try {
    const data = req.body;
    products.forEach((result) => {
      if (result.id == data["id"]) {
        res.send("The id is already exist");
        return;
      }
    });
    var newProduct = new Product(
      data["name"],
      data["price"],
      data["id"],
      data["photo"],
      data["rate"],
      data["reviewer"],
      data["category"],
      data["description"]
    );
    products.push(newProduct);
    res.send("data inserted successfully");
  } catch (error) {
    console.log("here is error", error);
    res.send({ message: "there is error", error: error });
  }
};

const getProducts = (req, res, next) => {
  try {
    res.send(products);
  } catch (error) {
    res.send({ message: "there is error", error: error });
  }
};

const getProduct = (req, res, next) => {
  try {
    let result = null;
    const id = req.params.id;

    // console.log("id : ", id);
    products.forEach((data) => {
      if (data.id == id) {
        res.send(data);
        // return;
      }
    });

    res.send("no Data found with " + id + " ");
  } catch (error) {
    res.send({ message: "there is error", error: error });
  }
};

const updateProduct = (req, res, next) => {
  try {
    const id = req.params.id;
    const newData = req.body;
    products.forEach((data) => {
      if (data.id == id) {
        var newProduct = new Product(
          newData["name"],
          newData["price"],
          newData["id"],
          newData["photo"],
          newData["rate"],
          newData["reviewer"]
        );
        let resultIndex = products.indexOf(data);
        products[resultIndex] = newData;
        res.send({
          message: "Successfully updated",
          data: products[resultIndex],
        });
      }
    });

    res.send("can't find an item with that id ");
  } catch (error) {
      console.log("error ",error);
    res.send({ message: "there is error", error: error });
  }
};

const deleteProduct = (req, res, next) => {
  try {
    const id = req.params.id;
    const data = req.body;

    //   let index = 0;
    //   let resultIndex = -1;

    products.forEach((item) => {
      if (item.id == id) {
        let resultIndex = products.indexOf(item);
        let temp = products[resultIndex];
        products.splice(resultIndex, 1);
        res.send({ message: "item deleted successfully ", data: temp });
        //   resultIndex = index;
      }
      // index = index + 1;
    });

    res.send("can't find an item with that id ");
  } catch (error) {
      console.log("error ",error)
    res.send({ message: "there is error", error: error });
  }
};

module.exports = {
  addProduct,
  getProducts,
  getProduct,
  updateProduct,
  deleteProduct,
};
