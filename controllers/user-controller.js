const db = require("../db1");
const User = require("../models/User");
const addUser = (req, res, next) => {
  const { email, password } = req.body;
  console.log("req.body : ", req.body);
  if (!email || !password) {
    res.send({
      message: "Please enter your email or password",
    });
  }
  var id = Math.random * 1000;
  var user = new User(id, email, password);
  db.push(user);
  res.send({
    message: "user successfully created",
    body: user,
  });
};

const getUsers = (req, res) => {
  res.send({
    message: "user successfully fetched",
    users: db,
  });
};

const getUser = (req, res) => {
  const id = req.params.id;


  const user = db.filter((user) => user.id === parseInt(id));

  res.send({ message: "user successfully fetched", user: user });
};



module.exports = {
  addUser,
  getUsers,
  getUser
};
